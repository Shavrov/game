import java.util.HashMap;
import java.util.Map;

public class Game {
    private Map<Integer, Integer> results = new HashMap<>();

    public void setResult(int key, int result) {
        results.put(key, result);
    }

    public int getResult(int key) {
        return results.get(key);
    }
}
