import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Player player = new Player();
        Play play = new Play();
        Game game = new Game();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите количество игроков");
        int countPlayers = scanner.nextInt();

        for (int i = 1; i <= countPlayers; i++) {
            System.out.println("Введите имя игроку № " + i);
            String namePlayer = scanner.next();
            player.saveNamePlayer(i, namePlayer);
        }

        String playMore = "y";
        while (playMore.equalsIgnoreCase("y")) {
            for (int i = 1; i <= countPlayers; i++) {
                game.setResult(i, play.rollOfTheDice());
                System.out.println("Игрок " + player.getNamePlayer(i) + " выбросил " + game.getResult(i));
            }

            boolean isValid = true;
            do {
                System.out.println("Играть еще? Y/N");
                playMore = scanner.next();
                if (playMore.equalsIgnoreCase("y") || playMore.equalsIgnoreCase("n")) {
                    isValid = false;
                }
            } while (isValid);
        }
    }

}
