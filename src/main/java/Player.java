import java.util.HashMap;
import java.util.Map;

public class Player {
    private String namePlayer;

    Map<Integer, String> namePlayers = new HashMap<>();

    public void saveNamePlayer(int count, String name) {
        namePlayers.put(count, name);
    }

    public String getNamePlayer(int key) {
        return namePlayers.get(key);
    }
}
